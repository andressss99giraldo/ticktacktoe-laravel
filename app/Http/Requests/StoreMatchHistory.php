<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\MatchModel;

class StoreMatchHistory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match_id' => 'required|exists:matches,id',
            'match_round_id' => 'required|exists:match_rounds,id',
            'row' => 'required|integer',
            'column' => 'required|integer',
            'player_type' => [
                'required',
                'integer',
                Rule::in(array_keys(MatchModel::getPlayerTypes())),
            ],
        ];
    }
}
