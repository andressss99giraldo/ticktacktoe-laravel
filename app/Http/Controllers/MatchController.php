<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MatchModel;
use App\Services\MatchService;
use App\Services\MatchRoundService;
use App\Services\MatchHistoryService;

class MatchController extends Controller
{
    public function index()
    {
        $matches =  MatchService::get();
        return view('match.index', compact(['matches']));
    }

    public function create()
    {
        $match = MatchService::create();
        return redirect(route('match.show', $match->id) . "?round={$match->round->id}&token={$match->token}");
    }

    public function show(Request $request, MatchModel $match)
    {
        $token = $request->query('token');
        $round = $request->query('round');
        $player_type = $request->query('player_type') ?? 1;
        abort_unless($token && $round, 404);

        $matchByToken = MatchService::getFirstByToken($token);
        abort_unless($match->id === $matchByToken->id, 404);

        $matchRound = MatchRoundService::find($round);
        abort_unless($matchRound && $matchRound->match_id === $match->id, 404);

        $matchSize = MatchModel::DEFAULT_SIZE;
        $firstPlayerType = MatchModel::FIRST_PLAYER_TYPE;
        $secondPlayerType = MatchModel::SECOND_PLAYER_TYPE;

        $preparedData = MatchHistoryService::getPreparedData($match, $round, $matchSize);



        return  view('match.show', compact(
            'match',
            'matchSize',
            'preparedData',
            'round',
            'firstPlayerType',
            'secondPlayerType',
            'player_type'
        ));
    }

    public function join($match_id)
    {
        $matchToJoin =  MatchService::findWithData($match_id);
        $match = $matchToJoin['match'];
        $preparedData = $matchToJoin['preparedData'];

        if (!$match || !isset($preparedData))
            return redirect()->back()->withErrors(['match' => 'The match id was not found']);
        if ($preparedData['isFullMatchField'])
            return redirect()->back()->withErrors(['match' => 'The match is over, start a new one']);
        // return $match;
        return redirect(route('match.show', $match->id) . "?round={$match->matchRoundLatest->id}&token={$match->token}&playerType=2");
    }

    public function updatePlayersName(Request $request, $match_id)
    {
        MatchService::updatePlayerNames($match_id, (object) $request->all());
        return response('ok', 200);
    }
}
