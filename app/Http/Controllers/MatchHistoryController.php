<?php

namespace App\Http\Controllers;

use App\Services\MatchHistoryService;
use App\Http\Requests\StoreMatchHistory;

class MatchHistoryController extends Controller
{
    public function store(StoreMatchHistory $request)
    {
        $matchData = MatchHistoryService::create($request->validated());
        return $matchData;
    }
}
