<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MatchModel;
use App\Services\MatchRoundService;

class MatchRoundsController extends Controller
{
    public function store($match_id)
    {
        $matchRound = MatchRoundService::create($match_id);
        return redirect(route('match.show', $matchRound->match->id) . "?round={$matchRound->id}&token={$matchRound->match->token}");
    }
}
