<?php

namespace App\Services;

use App\Models\MatchRound;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class MatchRoundService
{
    public function create(int $match_id): MatchRound
    {
        $matchRound = MatchRound::make([
            'match_id' => $match_id
        ]);

        return DB::transaction(function () use ($matchRound) {
            $matchRound->saveOrFail();

            return $matchRound;
        });
    }

    public static function find($id)
    {
        return MatchRound::find($id);
    }
}
