<?php

namespace App\Services;

use App\Models\MatchModel;
use App\Models\MatchHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Events\MatchPlayerNameChangedEvent;

class MatchHistoryService
{
    public static function create(array $data): array
    {
        $matchHistory = MatchHistory::make(Arr::only($data, [
            'match_id',
            'match_round_id',
            'row',
            'column',
            'player_type',
        ]));


        return DB::transaction(function () use ($matchHistory) {
            $matchHistory->saveOrFail();
            $preparedData = self::getPreparedData($matchHistory->match, $matchHistory->match_round_id, MatchModel::DEFAULT_SIZE);
            MatchPlayerNameChangedEvent::dispatch($matchHistory->match, $preparedData);
            return ['preparedData' => $preparedData, 'match' => $matchHistory->match];
        });
    }

    public static function getAllByMatch(MatchModel $match, int $roundId): Collection
    {
        $matchHistories = MatchHistory::where([
            'match_id' => $match->id,
            'match_round_id' => $roundId,
        ])->orderBy('id', 'asc');

        return $matchHistories->get();
    }

    public static function countByMatch(MatchModel $match, int $roundId): int
    {
        $matchHistories = MatchHistory::where([
            'match_id' => $match->id,
            'match_round_id' => $roundId,
        ]);

        return $matchHistories->count();
    }

    public static function isFullMatchField(int $countHistories, int $matchSize): bool
    {
        return $countHistories === ($matchSize * $matchSize);
    }

    public static function getPreparedData(MatchModel $match, int $roundId, int $matchSize): array
    {
        $matchHistories = [];
        $playerHistories = [];

        $playerType = null;

        $histories = self::getAllByMatch($match, $roundId);

        foreach ($histories as $history) {
            $playerType = $history->player_type;

            $matchHistories[$history->row][$history->column] = $playerType;
            $playerHistories[$playerType][$history->row][$history->column] = true;
        }

        $horizontalSuccess = self::getHorizontalSuccess($matchHistories, $playerHistories, $matchSize);
        $verticalSuccess = self::getVerticalSuccess($matchHistories, $playerHistories, $matchSize);

        $diagonalRightSuccess = self::getDiagonalRightSuccess($matchHistories, $playerHistories, $matchSize);
        $diagonalLeftSuccess = self::getDiagonalLeftSuccess($matchHistories, $playerHistories, $matchSize);

        $matchOver = self::isMatchOver($matchSize, $horizontalSuccess, $verticalSuccess, $diagonalRightSuccess, $diagonalLeftSuccess);

        $playerWinner = self::getPlayerWinner($matchSize, $matchHistories, $horizontalSuccess, $verticalSuccess, $diagonalRightSuccess, $diagonalLeftSuccess);
        $match_id = $match->id;

        $matchCountHistories = self::countByMatch($match, $roundId);

        $isFullMatchField = self::isFullMatchField($matchCountHistories, $matchSize) ||  $matchOver ?? 1;
        return compact(
            'playerType',
            'playerWinner',
            'matchHistories',
            'horizontalSuccess',
            'verticalSuccess',
            'diagonalRightSuccess',
            'diagonalLeftSuccess',
            'matchOver',
            'match_id',
            'isFullMatchField',
            'matchCountHistories'
        );
    }

    private function getPlayerWinner(
        int $matchSize,
        array $matchHistories,
        array $horizontalSuccess,
        array $verticalSuccess,
        array $diagonalRightSuccess,
        array $diagonalLeftSuccess
    ): ?int {
        for ($row = 1; $row <= $matchSize; $row++) {
            for ($col = 1; $col <= $matchSize; $col++) {
                if ($horizontalSuccess[$row] ?? null) {
                    return $matchHistories[$row][$col];
                } elseif ($verticalSuccess[$col] ?? null) {
                    return $matchHistories[$row][$col];
                } elseif ($diagonalRightSuccess[$row][$col] ?? null) {
                    return $matchHistories[$row][$col];
                } elseif ($diagonalLeftSuccess[$row][$col] ?? null) {
                    return $matchHistories[$row][$col];
                }
            }
        }

        return null;
    }

    private function isMatchOver(
        int $matchSize,
        array $horizontalSuccess,
        array $verticalSuccess,
        array $diagonalRightSuccess,
        array $diagonalLeftSuccess
    ): bool {
        $matchOver = false;

        for ($row = 1; $row <= $matchSize; $row++) {
            for ($col = 1; $col <= $matchSize; $col++) {
                if ($horizontalSuccess[$row] ?? null) {
                    $matchOver = true;
                } elseif ($verticalSuccess[$col] ?? null) {
                    $matchOver = true;
                } elseif ($diagonalRightSuccess[$row][$col] ?? null) {
                    $matchOver = true;
                } elseif ($diagonalLeftSuccess[$row][$col] ?? null) {
                    $matchOver = true;
                }
            }
        }

        return $matchOver;
    }

    private function getHorizontalSuccess(array $matchHistories, array $playerHistories, int $matchSize): array
    {
        $horizontalSuccess = [];

        for ($row = 1; $row <= $matchSize; $row++) {
            $horizontalSuccess[$row] = true;
            $firstCell = null;

            for ($col = 1; $col <= $matchSize; $col++) {
                if ($firstCell === null) {
                    $firstCell = $matchHistories[$row][$col] ?? false;
                }

                $cell = $playerHistories[$firstCell][$row][$col] ?? false;

                $horizontalSuccess[$row] = $horizontalSuccess[$row] && $cell;
            }
        }

        return $horizontalSuccess;
    }

    private function getVerticalSuccess(array $matchHistories, array $playerHistories, int $matchSize): array
    {
        $verticalSuccess = [];

        for ($col = 1; $col <= $matchSize; $col++) {
            $verticalSuccess[$col] = true;
            $firstCell = null;

            for ($row = 1; $row <= $matchSize; $row++) {
                if ($firstCell === null) {
                    $firstCell = $matchHistories[$row][$col] ?? false;
                }

                $cell = $playerHistories[$firstCell][$row][$col] ?? false;

                $verticalSuccess[$col] = $verticalSuccess[$col] && $cell;
            }
        }

        return $verticalSuccess;
    }

    private function getDiagonalRightSuccess(array $matchHistories, array $playerHistories, int $matchSize): array
    {
        $diagonalRightSuccess = [];
        $diagonalRight = 0;

        $firstCell = null;

        for ($row = 1; $row <= $matchSize; $row++) {
            for ($col = 1; $col <= $matchSize; $col++) {
                if ($row === $col) {
                    if ($firstCell === null) {
                        $firstCell = $matchHistories[$row][$col] ?? false;
                    }

                    if (!isset($diagonalRightSuccess[$row][$col])) {
                        $diagonalRightSuccess[$row][$col] = true;
                    }

                    $cell = $playerHistories[$firstCell][$row][$col] ?? false;

                    if ($cell) {
                        $diagonalRight++;
                    }

                    $diagonalRightSuccess[$row][$col] = $diagonalRightSuccess[$row][$col] && $cell;
                }
            }
        }

        if ($diagonalRight < $matchSize) {
            $diagonalRightSuccess = [];
        }

        return $diagonalRightSuccess;
    }

    private function getDiagonalLeftSuccess(array $matchHistories, array $playerHistories, int $matchSize): array
    {
        $diagonalLeftSuccess = [];
        $diagonalLeft = 0;

        $lastCell = null;

        for ($row = 1; $row <= $matchSize; $row++) {
            for ($col = 1; $col <= $matchSize; $col++) {
                if (($col === $matchSize && $row === 1) || ($col === ($matchSize - $row + 1))) {
                    if ($lastCell === null) {
                        $lastCell = $matchHistories[$row][$col] ?? false;
                    }

                    if (!isset($diagonalLeftSuccess[$row][$col])) {
                        $diagonalLeftSuccess[$row][$col] = true;
                    }

                    $cell = $playerHistories[$lastCell][$row][$col] ?? false;

                    if ($cell) {
                        $diagonalLeft++;
                    }

                    $diagonalLeftSuccess[$row][$col] = $diagonalLeftSuccess[$row][$col] && $cell;
                }
            }
        }

        if ($diagonalLeft < $matchSize) {
            $diagonalLeftSuccess = [];
        }

        return $diagonalLeftSuccess;
    }
}
