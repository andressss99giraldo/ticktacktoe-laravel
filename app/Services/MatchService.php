<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\MatchModel;
use Illuminate\Support\Facades\Redirect;
use App\Services\MatchHistoryService;

class MatchService
{
    public static function create(): MatchModel
    {
        $match = new MatchModel;
        $match->setToken();

        return DB::transaction(function () use ($match) {
            $match->saveOrFail();

            // $match->matchRounds()->create();
            $match->round = $match->matchRounds()->firstOrCreate();
            return $match;
        });
    }

    public function update(MatchModel $match, array $data): MatchModel
    {
        $match->update(Arr::only($data, [
            'second_player_name',
        ]));

        return $match;
    }

    public static function getFirstByToken(string $token): MatchModel
    {
        $match = MatchModel::where([
            'token' => $token
        ]);

        return $match->firstOrFail();
    }

    public static function findWithData($match_id): array
    {
        $preparedData = null;
        $match = MatchModel::find($match_id);

        if ($match)
            $preparedData = MatchHistoryService::getPreparedData($match, $match->matchRoundLatest->id, MatchModel::DEFAULT_SIZE);

        return compact('match', 'preparedData');
    }

    public static function updatePlayerNames($match_id, $match): MatchModel
    {
        $matchToUpdate = MatchModel::find($match_id);
        $matchToUpdate->first_player_name = $match->first_player_name;
        $matchToUpdate->second_player_name = $match->second_player_name;
        $matchToUpdate->saveOrFail();
        return $matchToUpdate;
    }

    public static function get()
    {
        return MatchModel::orderBy('id', 'desc')->get();
    }
}
