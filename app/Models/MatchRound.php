<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchRound extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id',
    ];

    public function match()
    {
        return $this->belongsTo(MatchModel::class);
    }

    public function matchHistories()
    {
        return $this->hasMany(MatchHistory::class, 'match_round_id', 'id');
    }
}
