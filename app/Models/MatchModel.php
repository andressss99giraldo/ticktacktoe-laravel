<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class MatchModel extends Model
{
    use HasFactory;

    public const DEFAULT_SIZE = 3;
    public const FIRST_PLAYER_TYPE = 1;
    public const SECOND_PLAYER_TYPE = 2;
    protected $table = 'matches';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_player_name',
        'second_player_name',
        'token',
    ];

    public static function generateToken(): string
    {
        return (string) Str::uuid();
    }

    public static function getPlayerTypes(?int $type = null)
    {
        $types = [
            self::FIRST_PLAYER_TYPE => 'x',
            self::SECOND_PLAYER_TYPE => 'o',
        ];

        if ($type !== null) {
            return $types[$type] ?? null;
        }

        return $types;
    }

    public function setToken(): void
    {
        $this->token = self::generateToken();
    }

    public function matchRounds()
    {
        return $this->hasMany(MatchRound::class, 'match_id', 'id');
    }

    public function countMatchRounds()
    {
        return $this->hasMany(MatchRound::class, 'match_id', 'id')->count();
    }

    public function matchRoundLatest()
    {
        return $this->hasOne(MatchRound::class, 'match_id', 'id')->latest();
    }
}
