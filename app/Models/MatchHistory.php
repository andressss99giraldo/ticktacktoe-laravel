<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchHistory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id',
        'match_round_id',
        'player_type',
        'row',
        'column',
    ];

    public function match()
    {
        return $this->belongsTo(MatchModel::class);
    }
}
