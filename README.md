<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## TickTackToe Laravel - VueJS - NodeJS

Esta es mi prueba para el cargo de Desarrollador Backend con Laravel.

Pasos para su despliegue:

-   composer install
-   cp .env.example .env
-   Establecer el nombre de la base de datos que se usuara en el archivo '.env'.
-   php artisan:migrate
-   php artisan serve

Levantar el servidor de NodeJS para WebSockets

-   cd WebSocketsServer
-   npm install
-   yarn start

Nota: Necesitara inicializar su servidor de Redis para el correcto funcionamiento de la aplicación.
