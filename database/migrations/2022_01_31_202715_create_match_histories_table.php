<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_histories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('match_id');
            $table->unsignedBigInteger('match_round_id');

            $table->foreign('match_id')->references('id')->on('matches')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('match_round_id')->references('id')->on('match_rounds')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->unsignedTinyInteger('player_type');

            $table->unsignedSmallInteger('row');

            $table->unsignedSmallInteger('column');

            $table->unique([
                'match_round_id',
                'player_type',
                'row',
                'column',
            ], 'match_histories_unique');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_histories');
    }
}
