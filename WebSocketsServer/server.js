const express = require("express");
const cors = require("cors");
const app = express();

const bodyParser = require("body-parser");
const port = 3000;

const corsOptions = {
    origin: "*",
    optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(
    cors({
        origin: "*",
        credentials: true,
    })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get("/", (req, res) => {
    res.send("What´s up?");
});

const server = app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`);
});

const io = require("socket.io").listen(server);
io.set("transports", ["websocket"]);
io.on("connection", function (socket) {
    console.log("A user connected");

    socket.on("match.join", function (match_id) {
        socket.join("match." + match_id);
        console.log("join match." + match_id);
    });

    socket.on("match.leave", function (match_id) {
        socket.leave("match." + match_id);
        console.log("leave match." + match_id);
    });

    socket.on("match.changed", function (data) {
        const {
            match: { id },
        } = data;
        io.to("match." + id).emit("match.changed", data);
    });

    //Whenever someone disconnects this piece of code executed
    socket.on("disconnect", function () {
        console.log("A user disconnected");
    });
});
