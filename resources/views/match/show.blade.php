@extends('layouts.app')
@section('content')
    <script>
       setTimeout(() => 
            window.event.$emit("matchData", {
                preparedData : @json($preparedData),
                match : @json($match),
                matchSize : {{$matchSize}},
            }
        ), 150);

    </script>

    <div class="container">

        <div class="d-flex justify-content-end m-3 align-items-center">
            ID del Juego  &nbsp;<span style="font-size: 1.5em">{{$match->id}}</span>
        </div>
  
        <div class="w-100" id="ticktack">

        <players-name></players-name>
            <tick-tack-toe></tick-tack-toe>
        </div>
        
        <hr>

        <div class="row">
            <div class="col-sm text-center pb-3">
                {{ link_to_route('matchRounds.store','Jugar de Nuevo', ['match_id'=> $match->id], ['class' => 'btn btn-primary']) }}
            </div>
            <div class="col-sm text-center pb-3">
                {{ link_to_route('match.create', 'Nueva Partida', [], ['class' => 'btn btn-success']) }}
            </div>
            <div class="col-sm text-center pb-3">

                {{ link_to_route('match.index', 'Home', [], ['class' => 'btn btn-info']) }}

            </div>
        </div>
    </div>


   

@endsection