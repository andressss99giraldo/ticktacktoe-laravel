@extends('layouts.app')
@section('content')
<div class="text-center" id="join">

    @if ($errors->any())

    <div class="alert alert-danger" role="alert">
        <p class="mb-0">{{ 'Errors' }}</p>
        <ul class="mb-0">

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>
    </div>

@endif

    <a href="{{ route('match.create') }}" class="btn btn-primary btn-lg">Nueva Partida</a>
    <join-match></join-match>
    {{-- <a href="{{ route('match.show',1) }}" class="btn btn-secondary btn-lg">Join to match</a> --}}

    <div class="container mt-4">
        <table class="table table-dark table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Jugadores</th>
                <th scope="col">Rondas Jugadas</th>
            </tr>
            </thead>
            <tbody>
{{-- {{$matches}} --}}
            @foreach ($matches as $match)
            <tr>
                <th scope="row">{{ $match->id }}</th>
                <td>{{ $match->first_player_name }} | {{ $match->second_player_name }}</td>
                <td>{{ $match->countMatchRounds() }}</td>
            </tr>
            @endforeach
</div>
   

@endsection