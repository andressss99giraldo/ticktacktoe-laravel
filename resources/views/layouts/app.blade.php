<!DOCTYPE html>
<html  lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- Styles CSS -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>

    <div class="title-container">
        <div class="title-content">
            <h1 class="text-center my-5 d-inherit">{{ config('app.name', 'Laravel') }}</h1>
            <img src="/img/tick-tack.jpg" width="73" />
        </div>
    </div>
       
        @yield('content')

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>