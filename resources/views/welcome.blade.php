@extends('layouts.app')
@section('content')
<div class="text-center">

    <a href="{{ route('match.create') }}" class="btn btn-primary btn-lg">New Match</a>
    <a href="{{ route('match.show',1) }}" class="btn btn-secondary btn-lg">Join to match</a>

</div>
@endSection
