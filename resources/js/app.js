require("./bootstrap");
window.Swal = require("sweetalert2");
window.Vue = require("vue").default;
Vue.component("tick-tack-toe", require("./components/TickTackToe.vue").default);
Vue.component("players-name", require("./components/PlayersName.vue").default);
Vue.component("join-match", require("./components/JoinMatch.vue").default);

window.event = new Vue();

if (document.querySelector("#ticktack"))
    new Vue({
        el: "#ticktack",
        delimiters: ["@{", "}"],
    });

if (document.querySelector("#join"))
    new Vue({
        el: "#join",
        delimiters: ["@{", "}"],
    });
