<?php

use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\MatchController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MatchRoundsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'index'])
    ->name('home');


Route::resource('match', MatchController::class);

Route::get('/match/join/{match_id}', [MatchController::class, 'join'])
    ->name('match.join');


Route::get('/match-round/create/{match_id}', [MatchRoundsController::class, 'store'])
    ->name('matchRounds.store');
